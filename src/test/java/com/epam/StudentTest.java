package com.epam;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class StudentTest {

    private final Map<String,Integer[]> marks = new HashMap<String, Integer[]>();


    @BeforeEach
    void initElements(){
        System.out.println("BeforeEach...");
        marks.put("2313",new Integer[]{2,3,1,3});
        //marks.put("01.032",null);
        marks.put("031923",new Integer[]{0,3,1,9,2,3});
    }

    @AfterEach
    void clearElements(){
        System.out.println("AfterEach...");
    }


    @Test
    void getMarks() {
        assertThrows(NullPointerException.class,()->new Student().getMarks(null));
            for (Map.Entry<String, Integer[]> value : marks.entrySet()) {
                String key = value.getKey();
                Integer[] stMarks = value.getValue();

                int i = 0;
                for (Integer m : stMarks) {
                    assertEquals(("" + key.charAt(i)), m.toString());
                    i++;
                }
            }

    }
}