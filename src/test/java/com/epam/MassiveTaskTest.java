package com.epam;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;


import static com.epam.MassiveTask.*;
import static org.junit.Assert.*;


class MassiveTaskTest {

    @BeforeAll
    static void beforeAll() {
        System.out.println("Before all..."+new MassiveTaskTest().toString());
    }

    @Test
    void areInBothTest() {
        int[][] firstComp = new int[][]{{1, 2, 3, 4, 5, 6, 7}, {1, 2, 3}, {2, 3, 4, 5}, {7, 8, 9, 0}};
        int[][] secondComp = new int[][]{{4, 5, 6, 7, 8, 9}, {0, 1}, {0, 1, 3}, {7, 8, 9, 0}};

        int[][] expectRes = new int[][]{{4, 5, 6, 7}, {1}, {3}, {7, 8, 9, 0}};


        for (int i = 0; i < firstComp.length; i++) {
            int[] result = areInBoth(firstComp[i], secondComp[i]);
            assertArrayEquals(result, expectRes[i]);
        }
    }

    @Test
    void areOnlyInFirstTest() {
        int[][] firstComp = new int[][]{{1, 2, 3, 4, 5, 6, 7}, {1, 2, 3}, {2, 3, 4, 5}, {7, 8, 9, 0}};
        int[][] secondComp = new int[][]{{4, 5, 6, 7, 8, 9}, {0, 1}, {0, 1, 3}, {7, 8, 9, 0}};

        int[][] expectRes = new int[][]{{1, 2, 3}, {2, 3}, {2, 4, 5}, {}};

        for (int i = 0; i < firstComp.length; i++) {
            int[] result = areOnlyInFirst(firstComp[i], secondComp[i]);
            assertArrayEquals(result, expectRes[i]);
        }

    }

    @Test
    void deleteMoreThenTwoTest() {
        int[][] firstComp = new int[][]{{1, 2, 3, 4, 5, 3, 2, 2, 5}, {6, 4, 5, 6}, {2, 3, 4, 5}, {0, 0, 0, 0}};

        int[][] expectRes = new int[][]{{1, 4}, {4, 5}, {2, 3, 4, 5}, {}};

        for (int i = 0; i < firstComp.length; i++) {
            int[] result = deleteMoreThenTwo(firstComp[i]);
            assertArrayEquals(result, expectRes[i]);
        }
    }

    @Test
    void deleteRepeatInRowTest() {

        int[][] firstComp = new int[][]{{1, 2, 2, 2, 3, 3, 4, 5, 5, 6, 6, 7}, {4, 5, 5, 6, 6}, {2, 3, 4, 5}, {0, 1, 0, 1, 0, 1, 0}};

        int[][] expectRes = new int[][]{{1, 2, 3, 4, 5, 6, 7}, {4, 5, 6}, {2, 3, 4, 5,}, {0, 1, 0, 1, 0, 1, 0}};

        for (int i = 0; i < firstComp.length; i++) {
            int[] result = deleteRepeatInRow(firstComp[i]);
            assertArrayEquals(result,expectRes[i]);
        }

    }


    @AfterAll
    static void afterAll() {
        System.out.println("After all..."+ new MassiveTaskTest().toString());
    }
}